package ua.od.atomspace.demoApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.demoApp.dto.UserDto;
import ua.od.atomspace.demoApp.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {
  private UserService userService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @PostMapping
  @ResponseBody
  public UserDto post(@RequestBody UserDto userDto) {
    UserDto response = userService.create(userDto);
    //there should be logger
    return response;
  }

  @GetMapping(params = {"subName"})
  @ResponseBody
  public List<UserDto> getBySubName(@RequestParam String subName) {
    List<UserDto> response = userService.get(subName);
    return response;
  }

  @GetMapping
  @ResponseBody
  public List<UserDto> list() {
    List<UserDto> response = userService.list();
    //there should be logger
    return response;
  }
}
