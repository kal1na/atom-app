package ua.od.atomspace.demoApp.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.demoApp.dao.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
  List<User> findAllByFirstNameLike(String firstName);
}
