package ua.od.atomspace.demoApp.service;

import ua.od.atomspace.demoApp.dto.UserDto;

import java.util.List;

public interface UserService {
  UserDto create(UserDto userDto);

  List<UserDto> list();

  List<UserDto> get(String subName);

  UserDto update();

  UserDto delete();
}
