package ua.od.atomspace.demoApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.od.atomspace.demoApp.dao.model.User;
import ua.od.atomspace.demoApp.dao.repository.UserRepository;
import ua.od.atomspace.demoApp.dto.UserDto;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Autowired
  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDto create(UserDto userDto) {
    User user = new User();
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setEmail(userDto.getEmail());
    user = userRepository.save(user);
    return userToDto.apply(user);
  }

  @Override
  public List<UserDto> list() {
    List<UserDto> userDtoList = new LinkedList<>();
    userRepository.findAll().forEach(user -> userDtoList.add(userToDto.apply(user)));
    return userDtoList;
  }

  @Override
  public List<UserDto> get(String subName) {
    List<UserDto> userDtoList = new LinkedList<>();
    userRepository.findAllByFirstNameLike("%" + subName + "%").forEach(user -> userDtoList.add(userToDto.apply(user)));
    return userDtoList;
  }

  @Override
  public UserDto update() {
    return null;
  }

  @Override
  public UserDto delete() {
    return null;
  }

  private Function<User, UserDto> userToDto = (user) -> {
    UserDto converted = new UserDto();
    converted.setId(user.getId());
    converted.setFirstName(user.getFirstName());
    converted.setLastName(user.getLastName());
    converted.setEmail(user.getEmail());
    return converted;
  };

}
